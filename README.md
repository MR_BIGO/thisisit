This is an application which is used for entertainment purposes. It includes 3 games and is connected to the FIREBASE authenticator.
The games include: "Tic-Tac-Toe", "Rock, Paper, Scissors" and "Guess The Number".
The game "Tic-Tac-Toe" let's you play with other person sitting next to you.
However, other games are played against a COMPUTER.

"Tic-Tac-Toe" - a game in which two players alternately put Xs and Os in compartments of a figure formed by two vertical lines crossing
two horizontal lines and each tries to get a row of three Xs or three Os before the opponent does.

"Rock, Paper, Scissors" - at the same time, two players display one of three symbols:
a rock, paper, or scissors. A rock beats scissors, scissors beat paper by cutting it, and paper beats rock by covering it.

"Guess The Number" - A computer chooses a random number between 1-200. Your goal is to guess it.
While playing, you will get hints by the computer whether your chosen number was higher or lower than the computer had originally chosen 
You win the game by guessing the correct number

