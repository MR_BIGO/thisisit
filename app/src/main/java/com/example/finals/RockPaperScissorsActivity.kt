package com.example.finals

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_rock_paper_scissors.*

class RockPaperScissorsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rock_paper_scissors)
        init()
    }

    private fun init() {
        RockButton.setOnClickListener {
            val Desision = 1

            val response = (1..3).random()

            if (Desision == response) {
                Toast.makeText(this, "The Computer Chose Rock, It is a draw", Toast.LENGTH_SHORT).show()
            }

            else if (response == 2) {
                Toast.makeText(this, "The Computer Chose Paper, You lost", Toast.LENGTH_SHORT).show()
            }

            else if (response == 3) {
                Toast.makeText(this, "The Computer Chose Scissors, You win", Toast.LENGTH_SHORT).show()
            }
        }

        PaperButton.setOnClickListener {
            val Desision = 2

            val response = (1..3).random()

            if (Desision == response) {
                Toast.makeText(this, "The Computer Chose Paper, It is a draw", Toast.LENGTH_SHORT).show()
            }

            else if (response == 1) {
                Toast.makeText(this, "The Computer Chose Rock, You Win", Toast.LENGTH_SHORT).show()
            }

            else if (response == 3) {
                Toast.makeText(this, "The Computer Chose Scissors, You lost", Toast.LENGTH_SHORT).show()
            }
        }

        ScissorsButton.setOnClickListener {
            val Desision = 3

            val response = (1..3).random()

            if (Desision == response) {
                Toast.makeText(this, "The Computer Chose Scissors, It is a draw", Toast.LENGTH_SHORT).show()
            }

            else if (response == 1) {
                Toast.makeText(this, "The Computer Chose Rock, You lost", Toast.LENGTH_SHORT).show()
            }

            else if (response == 2) {
                Toast.makeText(this, "The Computer Chose Paper, You win", Toast.LENGTH_SHORT).show()
            }

        }

    }


}






