package com.example.finals

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_guess_the_number.*

class GuessTheNumberActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guess_the_number)
        init()
    }

    private fun generateNumber() = (1..200).random()



    private fun init(){
        generatingButton.setOnClickListener {
            val randomnumber: Int =generateNumber()

            checkingButton.setOnClickListener {
                if (editText.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please, Enter a number", Toast.LENGTH_SHORT).show()
                }
                else if (randomnumber > editText.text.toString().toInt()) {
                    Toast.makeText(this, "Higher", Toast.LENGTH_SHORT).show()
                }
                else if (randomnumber < editText.text.toString().toInt()) {
                    Toast.makeText(this, "Lower", Toast.LENGTH_SHORT).show()
                }
                else if (randomnumber == editText.text.toString().toInt()) {
                    Toast.makeText(this, "Congratulations, you guessed the Number", Toast.LENGTH_SHORT).show()
                }

            }

        }
    }
}