package com.example.finals

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_signup.*

class signupActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        init()
    }

    private fun init() {
        auth = Firebase.auth
        signUpButton.setOnClickListener{
            signUp()

        }

    }
    private fun signUp() {
        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()
        val repeatPassword = repeatPasswordEditText.text.toString()

        if(email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty()){
            if(password == repeatPassword){
                progressBar.visibility= View.VISIBLE
                signUpButton.isClickable=false
                auth.createUserWithEmailAndPassword(email,password)
                    .addOnCompleteListener(this) { task ->
                        progressBar.visibility= View.GONE
                        signUpButton.isClickable=true
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("signup", "createUserWithEmail:Success!")
                            val user = auth.currentUser
                            val intent= Intent(this,loginActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)


                        } else {
                            // If sign in fails, display a message to the user.
                            Log.d("signup", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(baseContext, "Email format is not Correct",
                                Toast.LENGTH_SHORT).show()

                        }

                    }


            }else{
                Toast.makeText(this,"Password Is Not Correct", Toast.LENGTH_SHORT).show()

            }


        }else{
            Toast.makeText(this,"Please Fill all Fields", Toast.LENGTH_SHORT).show()
        }

    }
}

