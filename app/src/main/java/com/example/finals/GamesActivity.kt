package com.example.finals

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_games.*

class GamesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_games)
        init()
    }

    private fun init(){
        TicTacToeButton.setOnClickListener {
            val intent = Intent(this, TicTacToeActivity::class.java)
            startActivity(intent)
        }

        GuessTheNumberButton.setOnClickListener {
            val intent = Intent(this, GuessTheNumberActivity::class.java)
            startActivity(intent)
        }

        RockPaperScissorsButton.setOnClickListener {
            val intent = Intent(this, RockPaperScissorsActivity::class.java)
            startActivity(intent)
        }
    }

}